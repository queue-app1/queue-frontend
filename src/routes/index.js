import { Route } from "react-router-dom";
import EventList from "../components//Event/EventList";
import CreateEvent from "../components/Event/CreateEvent";
import Login from "../components/Login/Login";
import Registration from "../components/Registration/Register";
// eslint-disable-next-line import/no-anonymous-default-export
export default [
  <Route exact path="/" element={<EventList />} />,

  <Route exact path="/login" element={<Login />} />,

  <Route exact path="/register" element={<Registration />} />,

  <Route exact path="/create-event" element={<CreateEvent />} />,

  
];