import environment from "../environment/environment";
import { getAsync, postAsync } from "../_helpers/adapters";

export const login = async (post) => {
  try {
    const res = await postAsync(`${environment.apiUrl}/user/login`, post);
    return res;
  } catch (err) {
    console.log("under err");
    console.log(err);
    throw err;
  }
};

export const register = async (post) => {
  try {
    const res = await postAsync(`${environment.apiUrl}/user/`, post);
    return res;
  } catch (err) {
    throw err;
  }
};

export const isUserLoggedIn = () => {
  if (window.localStorage.getItem("authenticationToken")) {
    return true;
  } else {
    return false;
  }
};
