import environment from "../environment/environment";
import { getAsync, postAsync, updateAsync, deleteAsync } from "../_helpers/adapters";

export const createEvent = async (event) => {
  try {
    const res = await postAsync(`${environment.apiUrl}/event`, event);
    return res;
  } catch (err) {
    throw err;
  }
};

export const getEvents = async () => {
  try {
    const res = await getAsync(
      `${environment.apiUrl}/event`
    );
    return res;
  } catch (err) {
    throw err;
  }
};



export const getEventById = async (id) => {
  try {
    const res = await getAsync(
      `${environment.apiUrl}/event/${id}`
    );
    return res;
  } catch (err) {
    throw err;
  }
};

export const updateEvent = async (id, body) => {
    try {
      const res = await updateAsync(
        `${environment.apiUrl}/event/${id}`, body
      );
      return res;
    } catch (err) {
      throw err;
    }
};


export const deleteEvent = async (id ) => {
    try {
      const res = await deleteAsync(
        `${environment.apiUrl}/event/${id}`
      );
      return res;
    } catch (err) {
      throw err;
    }
};


