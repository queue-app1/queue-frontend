import environment from "../environment/environment.dev";
import { getAsync, postAsync, updateAsync, deleteAsync } from "../_helpers/adapters";

export const createQueue = async (queue) => {
  try {
    const res = await postAsync(`${environment.apiUrl}/queue`, queue);
    return res;
  } catch (err) {
    throw err;
  }
};

export const getPosts = async () => {
  try {
    const res = await getAsync(
      `${environment.apiUrl}/post`
    );
    return res;
  } catch (err) {
    throw err;
  }
};



export const getQueueById = async (id) => {
  try {
    const res = await getAsync(
      `${environment.apiUrl}/queue/${id}`
    );
    return res;
  } catch (err) {
    throw err;
  }
};

export const getQueueByEventId = async (eventId) => {
    try {
      const res = await getAsync(
        `${environment.apiUrl}/queue/event/${eventId}`
      );
      return res;
    } catch (err) {
      throw err;
    }
  };

  export const getQueueByUserId = async (userId) => {
    try {
      const res = await getAsync(
        `${environment.apiUrl}/queue/user/${userId}`
      );
      return res;
    } catch (err) {
      throw err;
    }
  };

export const updateQueue = async (id, body) => {
    try {
      const res = await updateAsync(
        `${environment.apiUrl}/queue/${id}`, body
      );
      return res;
    } catch (err) {
      throw err;
    }
};


export const deleteQueue = async (id ) => {
    try {
      const res = await deleteAsync(
        `${environment.apiUrl}/queue/${id}`
      );
      return res;
    } catch (err) {
      throw err;
    }
};


