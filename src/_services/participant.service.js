import environment from "../environment/environment.dev";
import { getAsync, postAsync, updateAsync, deleteAsync } from "../_helpers/adapters";

export const createParticipant = async (event) => {
  try {
    const res = await postAsync(`${environment.apiUrl}/participant`, event);
    return res;
  } catch (err) {
    throw err;
  }
};

export const getParticipant = async () => {
  try {
    const res = await getAsync(
      `${environment.apiUrl}/participant`
    );
    return res;
  } catch (err) {
    throw err;
  }
};



export const getParticipantById = async (id) => {
  try {
    const res = await getAsync(
      `${environment.apiUrl}/event/${id}`
    );
    return res;
  } catch (err) {
    throw err;
  }
};

export const getEventParticipants = async (eventId) => {
    try {
      const res = await getAsync(
        `${environment.apiUrl}//${eventId}`
      );
      return res;
    } catch (err) {
      throw err;
    }
  };




