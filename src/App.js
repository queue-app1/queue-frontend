import logo from './logo.svg';
import './App.css';
import Footer from "./components/Footer/Footer";
import { BrowserRouter as Router, Routes } from "react-router-dom";
import Header from "./components/Header/Header";
import routes from "./routes/index";

function App() {
  return (
    <>
    <Router>
      {/* sample header */}
      <Header />

      {/* sample body */}
      <div>
        <Routes>{routes}</Routes>
      </div>

      {/* sample footer */}
      <Footer />
    </Router>
  </>
    
  );
}

export default App;
