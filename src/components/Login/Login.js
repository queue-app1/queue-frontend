import React from 'react';
import '../../assets/css/main.css'
import {
    login
} from "../../_services/user.service";

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           userName : "",
           password: null
        }
    }

    registerUser = (e) =>{
        e.preventDefault();
        var user = {
            username:  this.state.username,
            password: this.state.password,
        };

        login(user).then((response)=>{
            if(response.success == true)
            {
                console.log("successfully logged in")
            }

        }).catch((err)=>{
            console.log(err)
        })

    }

    handleOnChange = (value) =>{
        this.setState(value);
    }

    render() {
        return (
            <>
                <div className="create-event-container row mt-2 mb-2" style={{width: "50%", marginLeft: "auto", marginRight: "auto"}}>
                    <form>
                        <div className="form-group">
                            <label for="exampleInputEventTitle1">User Name</label>
                            <input type="text" className="form-control" value={this.state.username} onChange={(e) => { this.handleOnChange({username: e.target.value})}} placeholder="Enter name" />

                        </div>
                        <div className="form-group">
                            <label for="exampleInputEventTitle1">Password</label>
                            <input type="password" className="form-control" placeholder="Enter password"  value={this.state.password} onChange={(e) => { this.handleOnChange({password: e.target.value})}} />
                        </div>
                        <br/>
                        <button type="button" className="btn btn-primary" onClick={this.registerUser}>Login</button>
                    </form>
                </div>

            </>
        )


    }
}