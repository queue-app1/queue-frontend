import React from 'react';
import '../../assets/css/main.css'
import {
    createEvent,
} from "../../_services/event.service";

export default class CreateEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           eventTitle : "",
           startDate: new Date()
        }
    }

    addEvent = (e) =>{
        e.preventDefault();
        var event = {
            title: this.state.eventTitle,
            startDate: new Date(this.state.startDate),
            endDate: null,
            has_ended: false
        };

        createEvent(event).then((response)=>{
            if(response.success == true)
            {
                console.log("successfully registered")
            }

        }).catch((err)=>{
            console.log(err)
        })

    }

    handleOnChange = (value) =>{
        this.setState(value);
    }

    render() {
        return (
            <>
                <div className="create-event-container row mt-2 mb-2" style={{width: "50%", marginLeft: "auto", marginRight: "auto"}}>
                    <form>
                        <div className="form-group">
                            <label for="exampleInputEventTitle1">Event Title</label>
                            <input type="text" className="form-control" value={this.state.eventTitle} onChange={(e) => { this.handleOnChange({eventTitle: e.target.value})}} placeholder="Enter event title" />

                        </div>
                        <div className="form-group">
                            <label for="exampleInputEventTitle1">Start Date</label>
                            <input type="date" className="form-control" placeholder="Enter event start date"  value={this.state.startDate} onChange={(e) => { this.handleOnChange({startDate: e.target.value})}} />
                        </div>
                        <br/>
                        <button type="button" className="btn btn-primary" onClick={this.addEvent}>Submit</button>
                    </form>
                </div>

            </>
        )


    }
}