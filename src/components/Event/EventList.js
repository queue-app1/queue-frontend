import React from 'react';
import {
    getEvents,
} from "../../_services/event.service";

import Modal from "react-bootstrap/Modal";

export default class EventsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            events: [],
            selectedEvent: null,
            showModal: false,
            displayName: ""
        }

    }

    componentDidMount() {
        console.log("under component did mount")
        this.fetchEvents();
    }

    fetchEvents = () => {

        getEvents().then((response) => {
            console.log(response);
            if (response.success == true) {
                this.setState({ events: response?.data })
            }
            else {
                console.log("error is");
                console.log(response)
            }


        }).catch((err) => {
            console.log(err)
        })

    }

    handleEventClick(i) {
        console.log("under handle click")
        this.state.selectedEvent = this.state.events[i];
        this.setState({ selectedEvent: this.state.events[i] });
        this.setState({ showModal: true });

    }

    hideModal() {
        this.setState({ showModal: false, selectedEvent: null });
    }

    registerEventParticipant = () => {

    }

    handleOnChange = (value) => {
        this.setState(value);
    }

    render() {
        return (
            <div>
                <div class="row m-5">
                    {
                        this.state.events && this.state.events.map((event, index) => {
                            return (<div class="col-md-4">
                                <div class="card" >
                                    <div class="card-body mr-2 p-3">
                                        <h5 class="card-title" style={{ padding: "10px", backgroundColor: "lightgray" }}>{event.title}</h5>
                                        <p class="card-text" style={{ padding: "10px" }}>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="btn btn-primary" onClick={() => this.handleEventClick(index)}>Attend</button>
                                    </div>
                                </div>
                            </div>)
                        })
                    }

                </div>
                <Modal show={this.state.showModal} >
                    <Modal.Header>Join Event {this.state.selectedEvent?.title}</Modal.Header>
                    <Modal.Body>
                        <form>
                            <div class="form-group">
                                <label>Display Name</label>
                                <input type="text" class="form-control" value={this.state.displayName} onchange={(e) => this.handleOnChange({ displayName: e.target.value })} />
                            </div>

                        </form>
                    </Modal.Body>
                    <Modal.Footer>
                        <div class="row">
                            <div className="col-md-6">
                                <button class="btn btn-primary float-left" onClick={() => this.registerEventParticipant}>Submit</button>
                            </div>
                            <div className="col-md-6">
                                <button class="btn btn-warning float-right" onClick={() => this.hideModal()}>Close</button>

                            </div>


                        </div>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}