import React from "react";

export default class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <footer className="bg-dark text-center text-white">
        {/* Grid container */}
        {/*  */}

        {/* Copyright */}
        <div
          className="text-center p-3"
          style={{ "background-color": "rgba(0, 0, 0, 0.2);" }}
        >
          © 2020 Copyright:
          <a className="text-white" href="https://mdbootstrap.com/">
            MDBootstrap.com
          </a>
        </div>
        {/* Copyright */}
      </footer>
    );
  }
}
